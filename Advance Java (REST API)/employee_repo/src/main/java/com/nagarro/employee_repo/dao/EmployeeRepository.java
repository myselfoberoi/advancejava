package com.nagarro.employee_repo.dao;

// Importing all the required packages
import org.springframework.data.jpa.repository.JpaRepository;
import com.nagarro.employee_repo.model.Employee;

// Creating dao class for employee
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	// Creating method to get employee by id
	public Employee findById(long id);
}
