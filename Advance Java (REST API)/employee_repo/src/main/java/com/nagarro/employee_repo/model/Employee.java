package com.nagarro.employee_repo.model;

// Importing all the required packages
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

// Creating employee class
@Entity
@Table(name = "emp_details")
public class Employee {

	// Properties of the employee class
	@Id
	private long empCode;
	private String empName;
	private String empLocation;
	private String empEmail;
	@Temporal(TemporalType.DATE)
	private Date empDob;

	// Parameterized constructor for the employee class
	public Employee(long empCode, String empName, String empLocation, String empEmail, Date empDob) {
		super();
		this.empCode = empCode;
		this.empName = empName;
		this.empLocation = empLocation;
		this.empEmail = empEmail;
		this.empDob = empDob;
	}

	// Default constructor for the employee class
	public Employee() {
		super();
	}

	// Overriding toString method for the employee
	@Override
	public String toString() {
		return "Employee [empCode=" + empCode + ", empName=" + empName + ", empLocation=" + empLocation + ", empEmail="
				+ empEmail + ", empDob=" + empDob + "]";
	}

	// Getter for employee code
	public long getEmpCode() {
		return empCode;
	}

	// Setter for employee code
	public void setEmpCode(long empCode) {
		this.empCode = empCode;
	}

	// Getter for the employee name
	public String getEmpName() {
		return empName;
	}

	// Setter for the employee name
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	// Getter for the location
	public String getEmpLocation() {
		return empLocation;
	}

	// Setter for the location
	public void setEmpLocation(String empLocation) {
		this.empLocation = empLocation;
	}

	// Getter for the email
	public String getEmpEmail() {
		return empEmail;
	}

	// Setter for the email
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	// Getter for the date of birth
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	public Date getEmpDob() {
		return empDob;
	}

	// Setter for the date of birth
	public void setEmpDob(Date empDob) {
		this.empDob = empDob;
	}
}
