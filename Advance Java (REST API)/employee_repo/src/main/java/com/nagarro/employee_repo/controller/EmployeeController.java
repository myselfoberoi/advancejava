package com.nagarro.employee_repo.controller;

// Importing all the required packages
import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.nagarro.employee_repo.model.Employee;
import com.nagarro.employee_repo.service.EmployeeService;
import com.nagarro.employee_repo.service.EmployeePDFExporter;

// Controller class for employee
@RestController
public class EmployeeController {

	// Autowiring employee service
	@Autowired
	private EmployeeService employeeService;

	// Handling get request for employees list
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> getEmployees() {
		List<Employee> list = this.employeeService.getAllEmployees();

		if (list.size() <= 0) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.of(Optional.of(list));
	}

	// Handling get request for an employee
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployee(@PathVariable("id") long id) {
		Employee employee = this.employeeService.getEmployeeById(id);
		if (employee == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		return ResponseEntity.of(Optional.of(employee));
	}

	// Handling post request to create an employee
	@PostMapping("/employees")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee) {
		Employee emp = null;
		try {
			emp = this.employeeService.addEmployee(employee);
			return ResponseEntity.status(HttpStatus.CREATED).body(emp);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	// Handling delete request for an employee
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Void> deleteEmployee(@PathVariable("id") long id) {
		try {
			this.employeeService.deleteEmployee(id);
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	// Handling put request for updating an employee
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) {
		try {
			this.employeeService.updateEmployee(employee);
			return ResponseEntity.ok().body(employee);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	// Handling get request for exporting pdf
	@GetMapping("/employees/export/pdf")
	public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=employees_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);
		List<Employee> employeeList = employeeService.getAllEmployees();
		EmployeePDFExporter exporter = new EmployeePDFExporter(employeeList);
		exporter.export(response);

	}
}
