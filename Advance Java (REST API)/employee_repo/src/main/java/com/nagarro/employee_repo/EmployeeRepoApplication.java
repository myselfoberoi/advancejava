package com.nagarro.employee_repo;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.nagarro.employee_repo.dao.EmployeeRepository;
import com.nagarro.employee_repo.model.Employee;

@SpringBootApplication
public class EmployeeRepoApplication {

	public static void main(String[] args) {
      ApplicationContext context = SpringApplication.run(EmployeeRepoApplication.class, args);
	  EmployeeRepository er = context.getBean(EmployeeRepository.class);
	  
	  // Saving some employee objects to test application
	  Employee e = new Employee();
      e.setEmpCode(100);
      e.setEmpName("Aman");
      e.setEmpLocation("Gurgaon");
      e.setEmpEmail("amansharma121@gmail.com");
      Date date = null;
      try{
    	  date = new SimpleDateFormat("dd-MM-yyyy").parse("12-09-2012");
    	  }catch(Exception ex)
      {
          ex.printStackTrace();
      }
      e.setEmpDob(date);
	  er.save(e);
	  
	  Employee e1 = new Employee();
      e1.setEmpCode(101);
      e1.setEmpName("Amar");
      e1.setEmpLocation("Jaipur");
      e1.setEmpEmail("amarshah101@gmail.com");
      date = null;
      try{
    	  date = new SimpleDateFormat("dd-MM-yyyy").parse("17-03-2005");
    	  }catch(Exception ex)
      {
          ex.printStackTrace();
      }
      e1.setEmpDob(date);
	  er.save(e1);
	
	  Employee e2 = new Employee();
      e2.setEmpCode(102);
      e2.setEmpName("Chandni");
      e2.setEmpLocation("Mohali");
      e2.setEmpEmail("chandnirawat94@gmail.com");
      date = null;
      try{
    	  date = new SimpleDateFormat("dd-MM-yyyy").parse("26-03-2017");
    	  }catch(Exception ex)
      {
          ex.printStackTrace();
      }
      e2.setEmpDob(date);
	  er.save(e2);
	}
}
