package com.nagarro.employee_repo.service;

// Importing all the required packages
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.nagarro.employee_repo.dao.EmployeeRepository;
import com.nagarro.employee_repo.model.*;

// Creating the service class for employee
@Component
public class EmployeeService {

	// Autowiring dao class
	@Autowired
	private EmployeeRepository employeeRepository;

	// Method to get all the employees
	public List<Employee> getAllEmployees() {
		List<Employee> list = (List<Employee>) this.employeeRepository.findAll();
		return list;
	}

	// Method to get the employee with the specified id
	public Employee getEmployeeById(long id) {
		Employee employee = null;
		try {
			employee = this.employeeRepository.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return employee;
	}

	// Method to create an employee
	public Employee addEmployee(Employee emp) {
		Employee e = this.employeeRepository.save(emp);
		return e;
	}

	// Method to delete an employee
	public void deleteEmployee(long id) {
		this.employeeRepository.deleteById(id);
	}

	// Method to update employee
	public void updateEmployee(Employee emp) {
		this.employeeRepository.save(emp);
	}
}
