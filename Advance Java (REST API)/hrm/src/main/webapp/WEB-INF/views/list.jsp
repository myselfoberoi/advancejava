<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employees List</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

	<!-- JSP for displaying list of employees -->

	<div class="container mt-3">
		<div class="col-md-12">
			<h6>
				<b>Employee Listings</b>
			</h6>
			<div>
				<p style="margin-left: 900px; font-size: 13px">
					<b>Welcome ${userName}</b>
					<button type="button" class="btn btn-sm btn-info ml-2"
						onclick="location.href='/hrm/'" style="font-size: 10px">Logout</button>
				</p>
			</div>
			<button type="button" class="btn btn-sm btn-danger"
				style="margin-left: 700px"
				onclick="location.href='/hrm/uploademployee'">Upload
				Employee Details</button>
			<button type="button" class="btn btn-sm btn-primary"
				onclick="location.href='http://localhost:8010/employees/export/pdf'">Download
				Employee Details</button>
			<table class="table mt-2 table-border">
				<thead class="thead-dark">
					<tr class="text-center">
						<th>Employee Code</th>
						<th>Employee Name</th>
						<th>Location</th>
						<th>Email</th>
						<th>Date Of Birth</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach items="${list}" var="p">
						<tr>
							<td class="text-center">${p.empCode}</td>
							<td class="text-center">${p.empName}</td>
							<td class="text-center">${p.empLocation}</td>
							<td class="text-center">${p.empEmail}</td>
							<td class="text-center"><fmt:formatDate type="date"
									value="${p.empDob}" /></td>
							<td>
								<button class="btn btn-sm btn-success" id="${p.empCode}"
									onclick="location.href='/hrm/edit?id='+this.id">edit</button>
								<button class="btn btn-sm btn-warning" id="${p.empCode}"
									onclick="location.href='/hrm/delete?id='+this.id">delete</button>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>