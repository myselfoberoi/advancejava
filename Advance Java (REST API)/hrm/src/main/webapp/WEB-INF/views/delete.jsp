<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Delete Confirmation Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

	<!-- Delete jsp for confirmation to delete employee -->

	<div class="container mt-3">
		<div class="col-md-12">
			<h6 class="d-flex justify-content-center">
				<b>Delete Confirmation</b>
			</h6>
			<table class="table mt-2 table-dark table-striped">
				<tbody>

					<tr>
						<td>Employee Code :</td>
						<td class="text-center">${employeeToDelete.empCode}</td>
					</tr>
					<tr>
						<td>Employee Name :</td>
						<td class="text-center">${employeeToDelete.empName}</td>
					</tr>
					<tr>
						<td>Location :</td>
						<td class="text-center">${employeeToDelete.empLocation}</td>
					</tr>
					<tr>
						<td>Email :</td>
						<td class="text-center">${employeeToDelete.empEmail}</td>
					</tr>
					<tr>
						<td>Date Of Birth :</td>
						<td class="text-center"><fmt:formatDate type="date"
								value="${employeeToDelete.empDob}" /></td>
					</tr>

					<tr>
						<td>
							<button style="margin-left: 470px" class="btn btn-sm btn-success"
								id="${employeeToDelete.empCode}"
								onclick="location.href='/hrm/deleteconfirmation?id='+this.id+'&status=y'">Delete</button>
							<button class="btn btn-sm btn-warning"
								id="${employeeToDelete.empCode}"
								onclick="location.href='/hrm/deleteconfirmation?id='+this.id+'&status=n'">Cancel</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>