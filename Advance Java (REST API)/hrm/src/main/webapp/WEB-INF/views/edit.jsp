<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<title>Edit Employee Information</title>
<script>
	var eCode, eName, eLocation, eDob, eEmail;
	function fillDetails() {
		document.getElementById('empCode').value = eCode;
		document.getElementById('empName').value = eName;
		document.getElementById('empLocation').value = eLocation;
		document.getElementById('empDob').value = eDob;
		document.getElementById('empEmail').value = eEmail;
	}
</script>
</head>
<body>

	<!-- JSP for editing employee information -->

	<div class="container w-50">
		<div class="mt-3">
			<p style="margin-left: 430px; font-size: 13px">
				<b>Welcome ${userName}</b>
				<button type="button" class="btn btn-sm btn-info ml-2"
					onclick="location.href='/hrm/'" style="font-size: 10px">Logout</button>
			</p>
		</div>
		<form name="editForm" method="POST" action="editemployee?status=y">
			<div
				class="container mt-5 d-flex justify-content-left text-light bg-secondary">
				Edit Employee Details</div>
			<div
				class="container mt-2 text-dark d-flex justify-content-left text-light bg-light">
				<label style="font-size: 15px">Employee Code</label><label
					style="margin-left: 16px">:</label><br> <input class="mt-1"
					type="text" id="empCode" name="empCode"
					style="height: 20px; margin-left: 48px; font-size: 13px; width: 200px" /><br>
			</div>

			<div
				class="container text-dark d-flex justify-content-left text-light bg-light">
				<label class="mt-2" style="font-size: 15px">Employee Name</label><label
					class="mt-1" style="margin-left: 12px">:</label><br> <input
					style="height: 20px; margin-left: 48px; margin-top: 10px; font-size: 13px; width: 200px"
					id="empName" type="text" name="empName" /><br>
			</div>

			<div
				class="container text-dark d-flex justify-content-left text-light bg-light">
				<label class="mt-2" style="font-size: 15px">Location </label><label
					class="mt-1" style="margin-left: 64px">:</label><br> <input
					style="height: 20px; margin-left: 49px; margin-top: 10px; font-size: 13px; width: 200px"
					id="empLocation" type="text" name="empLocation" /><br>
			</div>

			<div
				class="container text-dark d-flex justify-content-left text-light bg-light">
				<label class="mt-2" style="font-size: 15px">Email</label><label
					class="mt-1" style="margin-left: 85px">:</label> <input
					style="height: 20px; margin-left: 49px; margin-top: 10px; font-size: 13px; width: 200px"
					id="empEmail" type="text" name="empEmail" /><br>
			</div>

			<div
				class="container text-dark d-flex justify-content-left text-light bg-light">
				<label class="mt-2" style="font-size: 15px">Date of Birth </label><label
					class="mt-1" style="margin-left: 36px">:</label> <input
					style="height: 20px; margin-left: 50px; margin-top: 10px; font-size: 13px; width: 200px"
					id="empDob" type="date" value="2017-07-21" value=""
					min="1997-01-01" max="2030-12-31" name="empDob" /><br>
			</div>


			<div class="d-flex justify-content-center bg-secondary">
				<button type="submit" class="btn btn-outline-light btn-sm"
					style="margin-left: 400px; font-size: 12px">Save</button>
				<button type="button"
					onclick="location.href='/hrm/editemployee?status=n'"
					class="btn btn-outline-light btn-sm"
					style="margin-left: 10px; font-size: 12px">Cancel</button>
			</div>

		</form>
		<p style="margin-left: 190px; font-size: 13px" class="text-danger">
			<b> ${error} </b>
		</p>
	</div>

	<script>
		eCode = '${empToEdit.empCode}';
		eName = '${empToEdit.empName}';
		eEmail = '${empToEdit.empEmail}';
		eLocation = '${empToEdit.empLocation}';
		eDob = '${empToEdit.empDob}';
		fillDetails();
	</script>
</body>
</html>