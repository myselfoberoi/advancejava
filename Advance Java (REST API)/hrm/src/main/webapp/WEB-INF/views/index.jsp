<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HRM Login Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script>
var idVal,passwordVal;
function fillValues()
{
  if(idVal !== 'null'){
  document.getElementById('userId').value = idVal;	
  }
  if(passwordVal !== 'null'){
  document.getElementById('userPassword').value = passwordVal;
  }
}
</script>
</head>
<body>

	<!-- JSP for taking username and password -->

	<div class="container w-50">
		<!-- Login Form  -->
		<form name="LoginForm" method="POST" action="employeeslist">
			<div
				class="container mt-5 d-flex justify-content-left text-light bg-secondary">
				Login Form</div>
			<div
				class="container mt-2 text-dark d-flex justify-content-left text-light bg-light">
				<label style="font-size: 15px">User Id</label><label
					style="margin-left: 26px">:</label> <input class="mt-1" type="text"
					id="userId" name="userId"
					style="height: 20px; margin-left: 40px; font-size: 15px" /><br>
			</div>
			<div
				class="container text-dark d-flex justify-content-left text-light bg-light">
				<label class="mt-2" style="font-size: 15px">Password</label><label
					class="mt-2" style="margin-left: 12px">:</label> <input
					style="height: 20px; margin-left: 39px; margin-top: 10px; font-size: 15px"
					id="userPassword" type="password" name="userPassword" /><br>
			</div>

			<div class="d-flex justify-content-center bg-secondary text-center">
				<button type="submit" class="btn btn-outline-light btn-sm"
					style="margin-left: 400px; font-size: 12px">Login</button>
			</div>
		</form>

		<%-- JSP Tag to get the login status --%>

		<p style="font-size: 10px; margin-left: 125px" class="text-danger">
			<b>${error}</b>
		</p>

	</div>
	<%
	String idVal = (String) request.getAttribute("idValue");
	String passwordVal = (String) request.getAttribute("passwordValue");
	%>

	<script> 
          idVal = "<%=idVal%>";
          passwordVal = "<%=passwordVal%>
		";
		fillValues();
	</script>
</body>
</html>