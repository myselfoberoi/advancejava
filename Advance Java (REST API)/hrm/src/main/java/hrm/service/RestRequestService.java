package hrm.service;

// Importing all the required packages
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import hrm.model.Employee;

// Service class to send request to rest api
public class RestRequestService {

	// Final static properties
	private final static String EMP = "/employees";
	private final static String EMP_AND_ID = "/employees/{id}";
	private final static String SERVER_URI = "http://localhost:8010";
	private final static RestTemplate restTemplate = new RestTemplate();

	// Method to get all the employees
	public static List<Employee> getAllEmployee() {
		ResponseEntity<List<Employee>> empResponse = restTemplate.exchange(SERVER_URI + EMP, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Employee>>() {
				});
		List<Employee> emps = empResponse.getBody();
		return emps;
	}

	// Method to get the employee
	public static Employee getEmployee(String id) {
		Map<String, Long> params = new HashMap<>();
		params.put("id", Long.valueOf(id));
		Employee emp = restTemplate.getForObject(SERVER_URI + EMP_AND_ID, Employee.class, params);
		return emp;
	}

	// Method to create employee
	public static void createEmployee(Employee emp) {
		restTemplate.postForObject(SERVER_URI + EMP, emp, Employee.class);
	}

	// Method to delete employee
	public static void deleteEmployee(String id) {
		Map<String, Long> params = new HashMap<>();
		params.put("id", Long.valueOf(id));
		restTemplate.delete(SERVER_URI + EMP_AND_ID, params);
	}

	// Method to update employee
	public static void updateEmployee(Employee e) {
		Map<String, Long> params = new HashMap<>();
		params.put("id", e.getEmpCode());
		restTemplate.put(SERVER_URI + EMP_AND_ID, e, Employee.class, params);
	}
}
