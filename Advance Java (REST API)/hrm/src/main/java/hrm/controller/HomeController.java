package hrm.controller;

// Importing all the required packages
import hrm.model.Employee;
import hrm.service.RestRequestService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

// Creating controller class
@Controller
public class HomeController {

	// Mapping home method to redirect to index.jsp
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		return "index";
	}

	// Mapping employeeinput with employeelist
	@RequestMapping(value = "/employeeslist", method = { RequestMethod.POST, RequestMethod.GET })
	public String employeeinput(@RequestParam("userId") String name, @RequestParam("userPassword") String password,
			Model model, HttpServletRequest request) {

		// Validating name and password
		if (name != null) {
			name = name.trim();
		}
		if (password != null) {
			password = password.trim();
		}
		if ((name == "" || name == null) && (password == "" || password == null)) {
			model.addAttribute("error", "User id and password fields can't be empty");
			return "index";
		}
		if (name == "" || name == null) {
			model.addAttribute("error", "User id field can't be empty");
			model.addAttribute("passwordValue", password);
			return "index";
		}
		if (password == "" || password == null) {
			model.addAttribute("error", "Password field can't be empty");
			model.addAttribute("idValue", name);
			return "index";
		}

		int nameLength, passwordLength;
		nameLength = name.length();
		passwordLength = password.length();

		if ((nameLength < 5 || nameLength > 50) && (passwordLength < 5 || passwordLength > 50)) {
			model.addAttribute("error", "User id and password must have between (5-50) characters");
			model.addAttribute("idValue", name);
			model.addAttribute("passwordValue", password);
			return "index";
		}

		if (nameLength < 5 || nameLength > 50) {
			model.addAttribute("error", "User id must have between (5-50) characters");
			model.addAttribute("idValue", name);
			model.addAttribute("passwordValue", password);
			return "index";
		}

		if (passwordLength < 5 || passwordLength > 50) {
			model.addAttribute("error", "Password must have between (5-50) characters");
			model.addAttribute("idValue", name);
			model.addAttribute("passwordValue", password);
			return "index";
		}
		List<Employee> list = RestRequestService.getAllEmployee();
		model.addAttribute("list", list);
		request.getSession().setAttribute("userName", name);
		return "list";
	}

	// Delete method to map with /delete
	@RequestMapping(value = "/delete", method = { RequestMethod.POST, RequestMethod.GET })
	public String delete(@RequestParam("id") String id, Model model) {
		Employee e = RestRequestService.getEmployee(id);
		model.addAttribute("employeeToDelete", e);
		return "delete";
	}

	// Deleteconfirmation method to handle delete task
	@RequestMapping(value = "/deleteconfirmation", method = { RequestMethod.POST, RequestMethod.GET })
	public String deleteConfirmation(@RequestParam("status") String status, @RequestParam("id") String id,
			Model model) {
		if (status.equals("y")) {
			RestRequestService.deleteEmployee(id);
		}
		List<Employee> list = RestRequestService.getAllEmployee();
		model.addAttribute("list", list);
		return "list";
	}

	// Edit method to perform edit operation
	@RequestMapping(value = "/edit", method = { RequestMethod.POST, RequestMethod.GET })
	public String edit(@RequestParam("id") String id, Model model) {
		Employee e = RestRequestService.getEmployee(id);
		model.addAttribute("empToEdit", e);
		return "edit";
	}

	// EditPage method mapped with /editemployee
	@RequestMapping(value = "/editemployee", method = { RequestMethod.POST, RequestMethod.GET })
	public String editPage(@ModelAttribute Employee e, @RequestParam("status") String status, Model model) {
		if (status.equals("y") && (e.getEmpName() == "" || e.getEmpDob() == null || e.getEmpEmail() == ""
				|| e.getEmpLocation() == "" || e.getEmpCode() <= 0)) {
			model.addAttribute("empToEdit", e);
			model.addAttribute("error", "Some data is missing/invalid");
			return "edit";
		}
		if (status.equals("y")) {
			RestRequestService.updateEmployee(e);
		}
		List<Employee> list = RestRequestService.getAllEmployee();
		model.addAttribute("list", list);
		return "list";
	}

	// Create method to create employee
	@RequestMapping(value = "/uploademployee", method = { RequestMethod.POST, RequestMethod.GET })
	public String create(Model model) {
		Employee e = new Employee();
		e.setEmpCode(200);
		model.addAttribute("empToCreate", e);
		return "upload";
	}

	// Create employee to run after confirmation
	@RequestMapping(value = "/uploadconfirmation", method = { RequestMethod.POST, RequestMethod.GET })
	public String createemployee(@ModelAttribute Employee e, @RequestParam("status") String status, Model model) {
		if (status.equals("y") && (e.getEmpName() == "" || e.getEmpDob() == null || e.getEmpEmail() == ""
				|| e.getEmpLocation() == "" || e.getEmpCode() <= 0)) {
			model.addAttribute("error", "Some data is missing/invalid");
			model.addAttribute("empToCreate", e);
			return "upload";
		}
		if (status.equals("y")) {
			RestRequestService.createEmployee(e);
		}
		List<Employee> list = RestRequestService.getAllEmployee();
		model.addAttribute("list", list);
		return "list";
	}

	// Download method to get pdf of the employees data
	@RequestMapping(value = "/downloademployee", method = { RequestMethod.POST, RequestMethod.GET })
	public String download(Model model) {
		List<Employee> list = RestRequestService.getAllEmployee();
		model.addAttribute("list", list);
		return "list";
	}

	// Logout method to logout
	@RequestMapping(value = "/logout", method = { RequestMethod.POST, RequestMethod.GET })
	public String logout() {
		return "/";
	}

	// Date binder for specific format of date to be accepted
	@InitBinder
	private void dateBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}
}