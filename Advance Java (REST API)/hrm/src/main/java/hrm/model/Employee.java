package hrm.model;

// Importing all the required packages
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

// Creating employee class
public class Employee {

	// Properties of employee class
	private long empCode;
	private String empName;
	private String empLocation;
	private String empEmail;
	private Date empDob;

	// Parameterized constructor for employee
	public Employee(long empCode, String empName, String empLocation, String empEmail, Date empDob) {
		super();
		this.empCode = empCode;
		this.empName = empName;
		this.empLocation = empLocation;
		this.empEmail = empEmail;
		this.empDob = empDob;
	}

	// Default constructor for employee
	public Employee() {
		super();
	}

	// Overriding toString method for employee
	@Override
	public String toString() {
		return "Employee [empCode=" + empCode + ", empName=" + empName + ", empLocation=" + empLocation + ", empEmail="
				+ empEmail + ", empDob=" + empDob + "]";
	}

	// Getter method for code
	public long getEmpCode() {
		return empCode;
	}

	// Setter method for code
	public void setEmpCode(long empCode) {
		this.empCode = empCode;
	}

	// Getter method for name
	public String getEmpName() {
		return empName;
	}

	// Setter method for name
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	// Getter method for location
	public String getEmpLocation() {
		return empLocation;
	}

	// Setter method for location
	public void setEmpLocation(String empLocation) {
		this.empLocation = empLocation;
	}

	// Getter method for email
	public String getEmpEmail() {
		return empEmail;
	}

	// Setter method for email
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	// Getter method for date of birth
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	public Date getEmpDob() {
		return empDob;
	}

	// Setter method for date of birth
	public void setEmpDob(Date empDob) {
		this.empDob = empDob;
	}
}
