job("Blog-Build") {
    description "Builds Blog from master branch."
    parameters {
        stringParam('COMMIT', 'HEAD', 'Commit to build')
    }
}